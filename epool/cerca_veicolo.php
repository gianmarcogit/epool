<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="style.css">
<title>Epool</title>
</head>
<body>
<?php
  include("funzioni.php");
?>
<h2>CERCA UN VEICOLO PER PRENOTARE</h2>

<?php
$sql = "select veicolo.targa, veicolo.modello, veicolo.capienza, veicolo.tariffaferiale, veicolo.tariffafestiva, ".
        " area_sosta.indirizzo, area_sosta.citta ".
        " from veicolo ".
        " inner join area_sosta on area_sosta.latitudine=veicolo.latitudine".
        " and area_sosta.longitudine=veicolo.longitudine order by veicolo.targa;";

        try {
          $stmt = $conn->prepare($sql);
          $stmt->execute();
        } catch (PDOException $e) {
            echo $e;
            exit();
        }
        $result = $stmt->fetchAll();

print "<table border='1' width='80%'>\n";
print"<tr><th>Targa</th><th>Modello</th><th>Capienza</th><th>Tariffa Feriale</th><th>Tariffa Festiva</th><th>Indirizzo</th><th>Citta</th><th>Segnalazioni</th><th>Prenota</th>\n";

foreach ($result as $row) {
  $targa=$row["targa"];
  print"<tr>";
   print "<td>".$row["targa"]."</td><td>".$row["modello"]."</td><td>".$row["capienza"]."</td><td>".$row["tariffaferiale"]."</td><td>".$row["tariffafestiva"]."</td><td>".$row["indirizzo"]."</td><td>".$row["citta"]."</td><td><a href=visualizza_segnalazioni.php?targa=$targa>vai</a></td><td><a href=prenota.php?targa=$targa>vai</a></td>";
    print"</tr>";
}

print "</table>\n";

?>

<body>
</html>
