<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="style.css">
<title>Epool</title>
</head>
<body>
<?php
  include("funzioni.php");

  $targa="";
  if (isset($_GET["targa"])){
    $targa=$_GET["targa"];
  }

?>
<h2>Elenco segnalazioni degli utenti relative al veicono con targa <?=$targa?></h2>

<?php
$sql = "select DataSegnalazione, Titolo, Testo, Nome, Cognome  from segnalazione inner join utente  where utente.indirizzoemail=segnalazione.indirizzoemail  and targa='$targa' order by DataSegnalazione desc";

        try {
          $stmt = $conn->prepare($sql);
          $stmt->execute();
        } catch (PDOException $e) {
            echo $e;
            exit();
        }
        $result = $stmt->fetchAll();

print "<table border='1' width='80%'>\n";
print"<tr><th width='10%'>Data Segnalazione</th><th>Titolo</th><th width='50%'>Testo</th><th>Utente</th>\n";


foreach ($result as $row) {
  print"<tr>";
   print "<td>".$row["DataSegnalazione"]."</td><td>".$row["Titolo"]."</td><td>".$row["Testo"]."</td><td>".$row["Nome"]." ".$row["Cognome"]."</td>";
    print"</tr>";
}

print "</table>\n";
?>
<br/><br/>




<h2>Elenco segnalazioni delle società relative al veicono con targa <?=$targa?></h2>

<?php
$sql2 = "select DataSegnalazione, Titolo, Testo, Nome from segnalazione inner join societa where segnalazione.idsocieta=societa.idsocieta and targa='$targa' order by DataSegnalazione desc";

        try {
          $stmt = $conn->prepare($sql2);
          $stmt->execute();
        } catch (PDOException $e) {
            echo $e;
            exit();
        }
        $result = $stmt->fetchAll();

print "<table border='1' width='80%'>\n";
print"<tr><th width='10%'>Data Segnalazione</th><th>Titolo</th><th width='50%'>Testo</th><th>Società</th>\n";


foreach ($result as $row) {
  print"<tr>";
   print "<td>".$row["DataSegnalazione"]."</td><td>".$row["Titolo"]."</td><td>".$row["Testo"]."</td><td>".$row["Nome"]."</td>";
    print"</tr>";
}

print "</table>\n";
?>
<br/><br/>




<a href="inserisci_segnalazione.php?targa=<?=$targa?>" title="Inserisci Segnalazione" >INSERISCI NUOVA SEGNALAZIONE</a>
<body>
</html>
