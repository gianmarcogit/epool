<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="style.css">
<title>Epool</title>
</head>
<body>


<?php
  include("funzioni.php");

  $codicep=$_GET['codp'];

  // ricaviamo la capienza del veicolo
  $sql= " SELECT capienza FROM veicolo inner join prenotazione on veicolo.targa=prenotazione.targa "
        ." where prenotazione.codiceprenotazione=".$codicep;
    try {
      $stmt = $conn->prepare($sql);
      $stmt->execute();
    } catch (PDOException $e) {
        echo $e;
        exit();
    }
    $result = $stmt->fetchAll();
    foreach ($result as $row){
      $capienza=$row["capienza"];
    }




  // ricaviamo il titolare del veicolo
  $sql= "SELECT utente.nome, utente.cognome FROM tragitto inner join prenotazione_tragitto on tragitto.idtragitto=prenotazione_tragitto.idtragitto "
        ." inner join prenotazione on prenotazione.codiceprenotazione=prenotazione_tragitto.codiceprenotazione "
        ." inner join utente on utente.indirizzoemail = prenotazione.indirizzoemail where prenotazione_tragitto.codiceprenotazione=".$codicep;
    try {
      $stmt = $conn->prepare($sql);
      $stmt->execute();
    } catch (PDOException $e) {
        echo $e;
        exit();
    }
    $result = $stmt->fetchAll();
    foreach ($result as $row){
      $nome=$row["nome"];
      $cognome=$row["cognome"];
    }



  $sql= "select tragitto.idtragitto, tipologia, numerokm from prenotazione_tragitto inner join tragitto on tragitto.idtragitto=prenotazione_tragitto.idtragitto and codiceprenotazione=".$codicep." order by tragitto.idtragitto;";



try {
  $stmt = $conn->prepare($sql);
  $stmt->execute();
} catch (PDOException $e) {
    echo $e;
    exit();
}


print "<h3>PARTECIPA AL VIAGGIO</h3>";

$result = $stmt->fetchAll();
foreach ($result as $row){
  $idT=$row["idtragitto"];
  print "<p><b>Identificativo del tragitto:</b> ".$row["idtragitto"]."</p>";
  print "<p><b>Tipologia del tragitto:</b> ".$row["tipologia"]."</p>";
  print "<p><b>Numero Km del tragitto:</b> ".$row["numerokm"]."</p>";
  print "<p><b>Capienza veicolo:</b> ".$capienza."</p>";
}


$sql= "select ordine, tappa_tragitto.orario, tappa.citta, tappa.via,  tappa_tragitto.stato, tappa_tragitto.latitudine lati, tappa_tragitto.longitudine longe from tappa_tragitto inner join tappa on tappa_tragitto.latitudine=tappa.latitudine and tappa_tragitto.longitudine=tappa.longitudine and tappa_tragitto.idtragitto=".$idT." order by tappa_tragitto.ordine;";
try {
  $stmt = $conn->prepare($sql);
  $stmt->execute();
} catch (PDOException $e) {
    echo $e;
    exit();
}
$result = $stmt->fetchAll();
print "<h4> Lista delle tappe </h4>";
print "<table border='1' width='80%'>\n";
print"<tr><th>Ordine</th><th>Orario</th><th>Citta</th><th>Via</th><th>Enjoy</th><th>Partecipanti</th></tr>";
foreach ($result as $row) {

  $sqlPart= "select nome, cognome  from condividi_tappa inner join utente on condividi_tappa.indirizzoemail=utente.indirizzoemail and condividi_tappa.idtragitto=".$idT." and latitudine=".$row['lati']." and longitudine=".$row['longe'].";";

  try {
    $stmtPart = $conn->prepare($sqlPart);
    $stmtPart->execute();
  } catch (PDOException $e) {
      echo $e;
      exit();
  }
  $elenco_partecipanti="<b>".$nome." ".$cognome."</b>";
  $resultPart = $stmtPart->fetchAll();
  foreach ($resultPart as $rowPart) {
    $elenco_partecipanti=$elenco_partecipanti."<br/>".$rowPart["nome"]." ".$rowPart["cognome"];
  }
  if ($row['stato'] == 'A'){
    print "<tr><td>".$row["ordine"]."</td><td>".$row["orario"]."</td><td>".$row["citta"]."</td><td>".$row["via"]."</td><td><a href='unisciti_car_pooling.php?lati=".$row["lati"]."&longe=".$row["longe"]."&codp=$codicep'>Partecipa</a></td><td>".$elenco_partecipanti."</td></tr>";
  }else{
    print "<tr><td>".$row["ordine"]."</td><td>".$row["orario"]."</td><td>".$row["citta"]."</td><td>".$row["via"]."</td><td>Completo</td><td>".$elenco_partecipanti."</td></tr>";
  }
}


print "</table>\n";


if (isset($_GET['lati'])){
  $lati=$_GET['lati'];
  $longe=$_GET['longe'];

  $insert = "CALL INSERT_PARTECIPA(:IndirizzoEmail, :latitudine, :longitudine, :idtragitto)";

  try {
    if($lati != '' && $longe !='')
    {
      $stmt=$conn->prepare($insert);
      $stmt->bindParam(":IndirizzoEmail", $_SESSION['id_logged']);
      $stmt->bindValue(":latitudine", $lati);
      $stmt->bindValue(":longitudine", $longe);
      $stmt->bindValue(":idtragitto", $idT);
      $stmt->execute();

      ?>
       <h4>La tua partecipazione alla tappa e' avvenuta con successo!</h4>



     <?php
     header("Location: unisciti_car_pooling.php?codp=".$codicep);
    }

    } catch (PDOException $e) {
      exit();
    }


}

?>
<body>
</html>
