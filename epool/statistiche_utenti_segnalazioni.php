<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="style.css">
<title>Epool</title>
</head>
<body>
<?php
  include("funzioni.php");
?>
<h2>Classifica degli utenti più attivi</h2>
<?php
$sql= "select utente.indirizzoemail, utente.nome, cognome, count(idsegnalazione) as numero from utente left join segnalazione on utente.indirizzoemail=segnalazione.indirizzoemail group by utente.indirizzoemail, utente.nome, cognome; order by 4 desc ";

try {
  $stmt = $conn->prepare($sql);
  $stmt->execute();
} catch (PDOException $e) {
    echo $e;
    exit();
}
$result = $stmt->fetchAll();

print "<table border='1' width='80%'>\n";
print"<tr><th>Username</th><th>Nome</th><th>Cognome</th><th>Numero segnalazioni</th>";

foreach ($result as $row) {
   print "<tr><td>".$row["indirizzoemail"]."</td><td>".$row["nome"]."</td><td>".$row["cognome"]."</td><td>".$row["numero"]."</td></tr>";
}

print "</table>\n";
?>


<body>
</html>
