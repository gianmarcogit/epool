<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="style.css">
<title>Epool</title>
</head>
<body>

<?php
  include("funzioni.php");

function funz_ricarica($val){
  if($val==1)
    return "SI";
  return "NO";
}


  $targa="";
  if (isset($_GET["targa"])){
    $targa=$_GET["targa"];
  }
  $op="";
  if (isset($_GET["op"])){
    $op=$_GET["op"];
  }
  if (strcmp($op,"OK")!=0){

    $sql="select latitudine, longitudine, indirizzo, ricarica from area_sosta;";
    try {
      $stmt = $conn->prepare($sql);
      $stmt->execute();
    } catch (PDOException $e) {
        echo $e;
        exit();
    }
    $result = $stmt->fetchAll();

?>
<h2>Inserisci la tua prenotazione per il veicolo con targa <?=$targa?></h2>


<form name="prenota" method="post" action="prenota.php?op=OK">
<p>Data Inizio (aaaa-mm-gg) <input type="text" name="DataInizio" size="10"> -
Ora Inizio hh-mi <input type="text" name="OraInizio" size="6"></p>
<p>Data fine (aaaa-mm-gg) <input type="text" name="DataFine" size="10"> -
Ora fine hh-mi <input type="text" name="OraFine" size="6"></p>
<p>Note <br><textarea name="note" rows="6" cols="50" >Inserisci le tue note</textarea></p>
<p>Area sosta partenza <br>
  <select name="sosta_partenza">
    <?
      foreach ($result as $row) {
    ?>
        <option value="<?=$row["latitudine"]."-".$row["longitudine"]?>"><?=$row["indirizzo"]." ric:".funz_ricarica($row["ricarica"])?></option>
    <?
      }
    ?>
  </select>
</p>

<p>Area sosta arrivo<br>
  <select name="sosta_arrivo">
    <?
      foreach ($result as $row) {
    ?>
        <option value="<?=$row["latitudine"]."-".$row["longitudine"]?>"><?=$row["indirizzo"]." ric:".funz_ricarica($row["ricarica"])?></option>
    <?
      }
    ?>
  </select>
</p>

<p>Targa <br><input type="text" readonly="readonly" name="targa" value="<?=$targa?>" size="10"></p>

<input type="reset" name="Cancella" value="Cancella">
<input type="submit" name="Inserisci" value="Inserisci Prenotazione">

</form>



<?php
}else{





  $insert = "CALL INSERT_PRENOTAZIONE(:IndirizzoEmail, :DataInizio, :OraInizio, :DataFine, :OraFine, :Targa, :Note, :LatitudineArrivo, :LongitudineArrivo, :LatitudinePartenza, :LongitudinePartenza)";

  $sosta_partenza = explode("-",$_POST['sosta_partenza']);
  $sosta_arrivo = explode("-",$_POST['sosta_arrivo']);

  if($_POST['DataInizio'] != '' && $_POST['OraInizio'] !='' && $_POST['DataFine'] !='' && $_POST['OraFine'] != '')
  {

    $dataI = $_POST['DataInizio'];
    $dataF = $_POST['DataFine'];

    $adesso = date('Y/m/d', strtotime("now"));
    $inizio = date('Y/m/d',strtotime($dataI));
    $fine = date('Y/m/d',strtotime($dataF));




    $errore="";
    if ($inizio < $adesso){
      $errore = "- Data inizio prenotazione deve essere maggiore o uguale di oggi<br/>";
    }
    if ($inizio > $fine){
      $errore = $errore ."- Data inizio prenotazione deve essere minore o uguale della data di fine prenotazione<br/>";
    }
  


          $stmt=$conn->prepare($insert);
          $stmt->bindParam(":IndirizzoEmail", $_SESSION["username"]);
          $stmt->bindValue(":DataInizio", $_POST['DataInizio']);
          $stmt->bindValue(":OraInizio", $_POST['OraInizio']);
          $stmt->bindValue(":DataFine", $_POST['DataFine']);
          $stmt->bindValue(":OraFine", $_POST['OraFine']);
          $stmt->bindValue(":Targa", $_POST['targa']);
          $stmt->bindValue(":Note", $_POST['note']);
          $stmt->bindValue(":LatitudineArrivo", trim($sosta_arrivo[0]),PDO::PARAM_STR);
          $stmt->bindValue(":LongitudineArrivo", trim($sosta_partenza[1]),PDO::PARAM_STR);
          $stmt->bindValue(":LatitudinePartenza", trim($sosta_arrivo[0]),PDO::PARAM_STR);
          $stmt->bindValue(":LongitudinePartenza", trim($sosta_partenza[1]),PDO::PARAM_STR);

        try {
          $stmt->execute();
        } catch (PDOException $e) {
            echo $e;
            exit();
        }
      ?>
         <h3>La tua prenotazione e' stata registrata con successo!</h3>
    <?php
  }else{
      print "<h3>La prenotaizone non e' avvenuta per questi problemi:<h3><br>".$errore;



  }else{
    print "<h3>Non sono stati indicati tutti i campi necessario per la prenotazione (*)</h3>";
  }



}
?>


<body>
</html>
