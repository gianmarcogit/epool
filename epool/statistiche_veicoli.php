<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="style.css">
<title>Epool</title>
</head>
<body>
<?php
  include("funzioni.php");
?>
<h2>Classifica dei veicoli più prenotati</h2>
<?php
$sql= "select marca, modello, count(codiceprenotazione) as numero from veicolo left join prenotazione on veicolo.targa=prenotazione.targa group by marca, modello order by 3 desc;";

try {
  $stmt = $conn->prepare($sql);
  $stmt->execute();
} catch (PDOException $e) {
    echo $e;
    exit();
}
$result = $stmt->fetchAll();

print "<table border='1' width='80%'>\n";
print"<tr><th>Marca</th><th>Modello</th><th>Numero prenotazioni</th>";

foreach ($result as $row) {
   print "<tr><td>".$row["marca"]."</td><td>".$row["modello"]."</td><td>".$row["numero"]."</td></tr>";
}

print "</table>\n";
?>


<body>
</html>
